import React, { Component } from 'react';
import Layout from './Components/Layout/Layout'
import BB from './Container/BurgerBuilder/BurgerBuilder'
import {Route,Switch,withRouter,Redirect} from 'react-router-dom';
// import Aux from './HOC/Aux';
// import Burger from './Components/Burger/Burger'
import COut from './Container/Checkout/Checkout'
import Orders from './Container/Orders';
import Auth from './Container/Auth/Auth';
import Logout from './Container/Auth/Logout';

import {connect} from 'react-redux';
import * as acT from './store/actions/index'

class App extends Component {
state={
  value:true,
}

componentDidMount(){
  // setTimeout(()=>this.setState({value:false})
  //           ,5000)
  this.props.onOverall();
}

  render() {
    let route=(
      <Switch>
         <Route path='/' exact component={BB}/>
            <Route path='/auth' exact component={Auth}/>
            <Redirect to='/'/>
      </Switch>
    )

    if(this.props.isAuth){
      route=(
        <Switch>
         <Route path='/' exact component={BB}/>
         <Route path='/auth' exact component={Auth}/>

          <Route path='/checkout' component={COut} />
             
             <Route path='/logout' exact component={Logout}/>
  
             <Route path='/Orders' component={Orders}/>
            <Redirect to='/'/>

        </Switch>
      )
    }
    

    return (    
      <Layout>
          <Switch>
            {route}
          </Switch>
            
      </Layout>     
    );
  }
}
const mapStateToProps=(state)=>{
return{
  isAuth:state.auth.token,
}
}

const mapDispatchToProps=(dispatch)=>{
  return{
    onOverall :()=>dispatch(acT.authCheckStateOverall()),
  }
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(App));
