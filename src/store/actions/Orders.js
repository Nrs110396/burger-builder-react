import * as acT from './ActionTypes';
import axioss from '../../axios-or';
import axios from '../../axios-or' 

export const purchaseBurgerSuccess =(id,orderData)=>{
    return{
        type:acT.Order_Success,
        id:id,
        orderData:orderData,
    };

}

export const purchaseBurgerFailure =(error)=>{
    return{
        type:acT.Order_Error,
        error:error,
    };

}

export const LoadingBegin =()=>{
    return{
        type:acT.orderLoadingBegin,
    }
}
export const realPurchaser=(onlineIngredients,token)=>{
    return (
        dispatch=>{
            dispatch(LoadingBegin()),
            axioss.post('/orders.json?auth='+token,onlineIngredients)
            .then( (res)=>{
                console.log(onlineIngredients)
                console.log(res.data );

                dispatch(purchaseBurgerSuccess(res.data.name,onlineIngredients));
            }
            
        
        )
            .catch(error=>
                dispatch(purchaseBurgerFailure(error))
            
            );
    })
}

export const onPurchasedDone=()=>{
        return{
            type: acT.purchasedDone,

        }
}


//OrderTab

export const OrderTabSuccess=(Order)=>{
return{
    type:acT.FetchOrderSuccess,
    order:Order
}

}

export const OrderTabStart=()=>{
    return{
        type:acT.FetchOrderStart,
       
    }
    
    }

export const OrderTabFail=(error)=>{
        return{
            type:acT.FetchOrderFail,
            error:error
        }
        
        }

export const OrderFetchOrder=(loginedToken,userId)=>{
    return dispatch=>{
        dispatch(OrderTabStart());
        //orderBy filters FB
        const queryParam= '?auth='+ loginedToken + '&orderBy="userId"&equalTo="' + userId +'"';
        axios.get('/orders.json'+ queryParam )
        .then(res=>{
            let arrey= []
            for(let key in res.data){
                console.log(key)
                arrey.push(
                    { 
                        ...res.data[key],
                        id : key 
                     }
                            )
                            
                            }
           
            dispatch(OrderTabSuccess(arrey))
        }).catch(err=>{

                dispatch(OrderTabFail(err))
        })
    }
}