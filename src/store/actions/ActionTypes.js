export const Add_Ingredient = 'Add_Ingredient';
export const Remove_Ingredient = 'Remove_Ingredient';

export const Fetch_Ingredient='Fetch_Ingredient';
export const Any_Error= 'Any_Error';


//Contact Data

export const Order_Success = 'Order_Success' 
export const Order_Error = 'Order_Error' 
export const orderLoadingBegin = 'Order_Loading'

export const purchasedDone = 'PurchasedDone';

//orderTab

export const FetchOrderStart ='FetchOrderStart';
export const FetchOrderSuccess= 'FetchOrderSuccess';
export const FetchOrderFail ='FetchOrderFail';

//Auth

export const Auth_Start =  'Auth_Start'
export const Auth_Success =  'Auth_Success'
export const Auth_Fail =  'Auth_Fail';
export const Auth_Logout = 'Auth_Logout';

export const Set_Auth_Redirect = 'Set_Auth_Redirect';