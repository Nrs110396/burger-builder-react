import * as acT from './ActionTypes';
import axios from 'axios';

export const authStart=()=>{
    return{
        type:acT.Auth_Start,
    }
}
export const authSuccess=(token,userId)=>{
    console.log(token);
    return{
        type:acT.Auth_Success,
        token:token,
        userId:userId
    }
};
export const logout=()=>{
    localStorage.removeItem('expiry');
    localStorage.removeItem('token');
    localStorage.removeItem('userId');

    return{
        type:acT.Auth_Logout,
    }
}
export const authLogOut=(expiry)=>{
return dispatch=>{
    setTimeout(()=>dispatch(logout()),expiry*1000);
    
}
}
export const authError=(error)=>{
    return{
        type:acT.Auth_Fail,
        error :error
    }
};
export const auth =(email,pass,signUp)=>{
    const mailDetails={
        email:email,
        password:pass,
        returnSecureToken:true,
    };

    let url ='https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyBiYaEdRQZMH8ZvG31m1QbYFZRgu4nUiD4'
  if(!signUp)
  {
      url='https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyBiYaEdRQZMH8ZvG31m1QbYFZRgu4nUiD4'
  }
    console.log(mailDetails)
    return dispatch=>{
        dispatch(authStart());
        axios.post(url,
            mailDetails)
       .then(res=>{
        console.log(res)   

        const expiryTime = new Date(new Date().getTime() + res.data.expiresIn * 1000);
        
        localStorage.setItem('token',res.data.idToken);
        localStorage.setItem('expiry',expiryTime);
        localStorage.setItem('userId',res.data.localId);
        
        dispatch(authSuccess(res.data.idToken,res.data.localId));
        dispatch(authLogOut(res.data.expiresIn));
    })
        
       .catch(err=>{
        console.log(err)  ; 
        dispatch(authError(err.response.data.error.message))})
    }
}

export const setAuthRedirect=(path)=>{
    return{
        type:acT.Set_Auth_Redirect,
        path:path,
    }
}

export const authCheckStateOverall =()=>{
return dispatch=>{
    const token = localStorage.getItem('token');
    if(!token){
        dispatch(logout());
    }
    else{
        const userID=localStorage.getItem('userId');
        const expiryD=new Date(localStorage.getItem('expiry'));
        if(expiryD >new Date){
            dispatch(authSuccess(token,userID));
        dispatch(authLogOut((expiryD.getTime()-new Date().getTime()/1000 ) )  );
        console.log((expiryD.getTime()-new Date().getTime()/1000 ));
            
        }
        else{
            dispatch(logout());
        }
    }
}
}