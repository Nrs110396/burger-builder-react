import * as acT from './ActionTypes'
import axioss from '../../axios-or'


export const addIngre=(ingrename)=>{
return{
       ingredientNAme:ingrename,
       type:acT.Add_Ingredient,
       error:false,
}

}

export const removeIngre=(ingrename)=>{
    return{
       type:acT.Remove_Ingredient,
       ingredientNAme:ingrename
    }
    
    }

export const initialIngre=(dataOne)=>{
    return {
        type:acT.Fetch_Ingredient,
        ingre:dataOne
    }
}

export const anyError=()=>{
return {
    type:acT.Any_Error,
    error:true
}

}
export const ingreFetcher=()=>{
    return (dispatch=>{
        axioss.get('https://burger-builder-86930.firebaseio.com/ingredients.json')
    .then(res=>{
        console.log(res.data)
        dispatch(initialIngre(res.data))}
    )
    .catch(error=>{
        dispatch(anyError())
       
    })
    })
}