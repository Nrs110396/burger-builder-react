import * as acT from '../actions/ActionTypes';

const inState= {
    orders : [],
    loading: false,
}
const reducer =(state=inState,action)=>{
    let newOrder = {
        ...action.orderData,
        id:action.id 
    }
    switch(action.type){
        case acT.purchasedDone:
        return{
            ...state,
            purchased:false,
        }
        
        case acT.Order_Success:
        return{
            ...state,
            loading:false,
            purchased:true,

            orders:state.orders.concat(newOrder)
        };
        case acT.Order_Error:
        return{
            ...state,
            loading:false,
        };
        case acT.orderLoadingBegin:
        return{
            ...state,
            loading:true,
        }

        case acT.FetchOrderSuccess:
        return{ 
            ...state,
            loading:false,

            orders:action.order,
        }
        case acT.FetchOrderFail:
        return{ 
            ...state,
            loading:false,
            orders:action.order,
        }
        case acT.FetchOrderStart:
        return{ 
            ...state,
            loading:true ,
            orders:action.order,
        }

    }
return state;
}

export default reducer;