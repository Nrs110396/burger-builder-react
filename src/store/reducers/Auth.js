import * as acT from '../actions/ActionTypes';
const inState={
    token:null,
    userId:null,
    loading:false,
    error:null,
    authRedirect:'/',
}
const Auth=(state=inState,action)=>{
    switch(action.type){
        case acT.Auth_Start:
            return{
                ...state,
                loading:true,
                error:null,
            }
        case acT.Auth_Success:
        return{
            ...state,
            loading:false,
            error:null,
            token:action.token,
            userId:action.userId,
        }
        case acT.Auth_Fail:
        return{
            ...state,
            loading:false,
            error:action.error,
            
        }
        case acT.Auth_Logout:
        return{
            ...state,
            userId:null,
            token:null,
        }
        case acT.Set_Auth_Redirect:
        return{
            ...state,
            authRedirect:action.path,
        }
        default:
        return state

    }

}

export default Auth