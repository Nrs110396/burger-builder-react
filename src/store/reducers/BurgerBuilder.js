import * as acT from '../actions/ActionTypes';
// import { throws } from 'assert';

const initialState={
    ingredient :null,
    total:40,
    error:false,
    building:false,
}

const Price = {
    cheese:5.5,
    salad:2.5,
    bacon:6.2,
    meat:7.8,
}

const adder=(state,action)=>{
return {
    ...state,
    ingredient:{
        ...state.ingredient,
        [action.ingredientNAme]: state.ingredient[action.ingredientNAme] +1
    },
    building:true,
    total:state.total+ Price[action.ingredientNAme]
    
};
}
const reducer=(state=initialState,action)=>{

    switch(action.type){
        case 'Add_Ingredient':
        return adder(state,action);
        case acT.Remove_Ingredient:
        return{
            ...state,
            ingredient:{
                ...state.ingredient,
                [action.ingredientNAme]: state.ingredient[action.ingredientNAme] -1
                
            },
            total:state.total- Price[action.ingredientNAme],
    building:true,


        };
        case acT.Fetch_Ingredient:
        return{
            ...state,
            ingredient: action.ingre,
            error:false,
            total:40,
            building:false,
        }
        case acT.Any_Error:
        return{
            ...state,
            error:action.error,
        }

    }

return state;
    

}

export default reducer;