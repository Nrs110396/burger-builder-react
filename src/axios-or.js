import axios from 'axios';

let instance = axios.create({
    baseURL: 'https://burger-builder-86930.firebaseio.com/'
});

export default instance; 