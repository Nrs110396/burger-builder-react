import React,{Component} from 'react';
import eStyles from './Order.css';
import axios from  '../../axios-or';
import wEH from '../../HOC/withErrorHandler/withErrorHandler'
class Order extends Component{
    
    render(){
       let eachOrder=[];
       for(let i in this.props.ingredients){
           eachOrder.push({label:i,amount:this.props.ingredients[i]})
           
       }
       console.log(eachOrder);
       let printer = eachOrder.map(res=>(<span ley={res.amount}
       style={{color:'rgb(0,47,54)',fontsize:'1px'}}>
       {res.label}({res.amount})</span>)
       )
        return(
<div className={eStyles.Order} style={{color:'rgb(20,36,38)'}}>
    <h4>Ingredients: {printer} </h4>
    <h5>Price: {Number.parseFloat(this.props.tprice).toFixed(2)}RS</h5>
</div>

        )
    }
}

export default Order;