import React from 'react';
import Burger from '../../Burger/Burger';
import eStyles from './CheckoutSummary.css';
import Button from '../../UI/Button/Button'
const CheckoutSummary=(props)=>{
    
    return(
    <div className={eStyles.Checkoutsummary}>
        <h1>Hope you enjoy this delicious burger..!!!</h1>
        <div className={eStyles.bSize}>
        <Burger ingredients={ props.ingredients}/>
</div>
<Button buttonType='Danger' whenClicked={props.cancelH} >CANCEL</Button>
<Button buttonType='Success' whenClicked={props.confirmH}>CONFIRM</Button>

    </div>)

}
export default CheckoutSummary;