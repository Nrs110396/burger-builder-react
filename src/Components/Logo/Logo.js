import React from 'react'
import eStyles from './Logo.css'
import logoImage from '../../Assets/BurgerLogo/BurgerLogo127.png'
const logo =()=>(

    <div className={eStyles.Logo}>
    <img src={logoImage} alt='LOGO'/>
    
    </div>
)
export default logo