import React from 'react';
import eStyles from './Input.css'
 
const inputs =(props)=>{
    let inputSelector = null;
    let Checker=true;
   
    let Styles = [eStyles.InputEle];
    
    
   Checker= props.isInValid;
     
     if(!Checker && props.ShouldValidateForDropdown && props.touched){
        Styles.push(eStyles.NotFilled);
    }
    

    switch(props.elementttype){
        case('input'):
        inputSelector = <input {...props.elementtconfig} 
         onChange={props.cChanged}
         className={Styles.join(' ')} 
        value={props.value}/>;
        break;
        case('textArea'):
        inputSelector = <textarea {...props.elementtconfig} 
        onChange={props.cChanged}        
        className={Styles.join(' ')}
        value={props.value}/>;
        break;
        case('select'):
        inputSelector = (<select  className={Styles.join(' ')}
         onChange={props.cChanged}
          value={props.value}> 
                        {
                            props.elementtconfig.options.map(i=>{
                           return(<option value={i.value}>{i.dispValue}</option>)
                        }
                        )
                        }
        </select>)
        break;
        default:
        console.log(props.value) 
         inputSelector = <input  {...props.elementtconfig} 
         onChange={props.cChanged}
        className={Styles.join(' ')}
        key={props.value}
        value={props.value}/>;
        break;


    }
return(
<div className={eStyles.Input}>
    <label className={eStyles.Label}>
    {props.name}
    </label>
    {inputSelector}
        </div>
)

}
export default inputs