 import React,{Component} from 'react';

 import Aux from '../../HOC/Aux'
 import scss from './Layout.css'
 import ToolBar from '../Navigation/Toolbar/Toolbar';
 import SideDrawer from '../Navigation/SideDrawer/SideDrawer';
 import {connect} from 'react-redux';
class Layout extends Component{
    state={
      SideDrawerShow:false,      
    }

    MenuClicker =()=>{

        this.setState({
            SideDrawerShow : true,
        })
    }

    SideDrawerOnclick=()=>{
        this.setState({
            SideDrawerShow : false,
        })
    }

   render(){ return(
        <Aux>
    
        <ToolBar 
        menuClicked={this.MenuClicker}
        isLogout={this.props.token}
        />
        <SideDrawer fun={this.SideDrawerOnclick} 
        click={this.state.SideDrawerShow}/>
      
     <main className={scss.content}>{this.props.children}</main>
    
     
 </Aux>)}
}
 
 const mapStateToProps=(state)=>{
    return{
        token:state.auth.token,
    }
 }

 

export default connect(mapStateToProps)(Layout);