import React from 'react'
import Logo from '../../Logo/Logo'
import NavigationItems from '../NavigationItems/NavigationItems'
import eStyles from './SideDrawer.css'
import BackDrop from '../../UI/BackDrop/BackDrop'
const sideDrawer=(props)=>{
    let aClasses = [eStyles.SideDrawer,eStyles.Close];
    if(props.click){
        aClasses = [eStyles.SideDrawer,eStyles.Open];
    }
return(
    <div>
    <BackDrop show={props.click} clickOut={props.fun}/>
    <div className={aClasses.join(' ')}
     onClick={props.fun}>
        <div className={eStyles.Logo}>
            <Logo/>
        </div>

        <NavigationItems/>
    </div>
    </div>

)


}

export default sideDrawer  