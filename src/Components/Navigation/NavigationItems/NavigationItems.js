import React,{Component} from 'react';
import NavigationItem from './NavigationItem/NavigationItem'
import eStyles from './NavigationItems.css';
import {connect} from 'react-redux'
class NavigationItems extends Component{
    render(){
    return(<ul className={eStyles.NavigationItems}>
            <NavigationItem link='/'>   Burger Builder </NavigationItem>
            {this.props.token ?<NavigationItem link='/Orders'>  Orders   </NavigationItem> :
            null}
            {!this.props.token ? 
            <NavigationItem link='/auth'>  Sign up   </NavigationItem> :
            <NavigationItem link='/logout'>  Logout  </NavigationItem>}
            

    </ul>)}
}
const mapStateToProps =(state)=>{
    return{
        token:state.auth.token,}
}

export default connect(mapStateToProps)(NavigationItems)