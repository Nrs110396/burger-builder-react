import React from 'react';
import eStyles from './NavigationItem.css'
import {NavLink} from 'react-router-dom'
const navigationItem =(props)=>{
    return(<li className={eStyles.NavigationItem}>
        <NavLink to={props.link} 
        activeClassName={eStyles.active} 
        exact> 
                         {props.children}  
        </NavLink>
        
        </li>)

}

export default navigationItem