import React from 'react'
import eStyles from './Toolbar.css'
import Logo from '../../Logo/Logo'
import Nav from '../NavigationItems/NavigationItems'
import Menu from './Menu'
const toolBar =(props)=>(

    <header className={eStyles.ToolBar}>
        <Menu menuClick={props.menuClicked}>Menu</Menu>
        <div className={eStyles.Logo}>
        <Logo/>
        </div>
        <nav className={eStyles.Desktop}>
            <Nav isLogout={props.isLogout}/>
            </nav >
    </header>
)

export default toolBar