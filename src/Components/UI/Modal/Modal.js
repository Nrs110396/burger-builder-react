import React,{Component} from 'react';
import estyles from './Modal.css';
import BackDrop from '../BackDrop/BackDrop'
class Modal extends Component{
    shouldComponentUpdate(nProps,nState){
        return nProps.show !== this.props.show || nProps.children !== this.props.children
    }
    
render(){

return( 
    <div>
    <BackDrop clickOut={this.props.clickOut}
    show={this.props.show}></BackDrop>
    
   <div className={estyles.Modal}
        style={{
            transition: this.props.show ? 'translateY(0)' : 'translateY(-100)',
            opacity: this.props.show ? '1' : '0'
        }}
    >
    {/* {console.log(props.show)} */}
        {this.props.children}
    </div>

        
    </div>



        )

}

}

export default Modal