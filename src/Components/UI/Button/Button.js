import React from  'react';
import eStyles from './Button.css'


const button =(props)=>(
<button disabled={props.disabled}
onClick={props.whenClicked} 
className={[eStyles.Button,eStyles[props.buttonType]].join(' ')}>
 
 {    props.children}
 
 </button>
)

export default button; 