import React from 'react';
import eStyles from './BackDrop.css'

const backDrop=(props)=>{return(

    props.show ? <div className={eStyles.BackDrop} 
    onClick={props.clickOut}></div> : null

)}
export default backDrop;