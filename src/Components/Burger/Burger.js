import React from 'react';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';
import estyles from './Burger.css'
const burger =(props)=>{
    let newIngredients = Object.keys(props.ingredients).map(isKey=>{
        return[...Array(props.ingredients[isKey])].map((_,i)=>{
            return<BurgerIngredient type={isKey} key={i+isKey}/>;
        })
    }).reduce((arr,ele)=>{
        return arr.concat(ele)
    },[]
);

   if (newIngredients.length === 0){
       newIngredients =<p>Please start Ordering..!</p>
    }

    

 return(
     <div className={estyles.burger} > 
         <BurgerIngredient type='burger-top'/>
         {newIngredients}
         {/* <BurgerIngredient type='bacon'/> */}

         <BurgerIngredient type='burger-bottom'/>


     </div>
 )

}

export default burger;