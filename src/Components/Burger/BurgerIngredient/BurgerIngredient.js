import BurgerIngredient from './BurgerIngredient.css';
import React,{Component} from 'react';
import Proptypes from 'prop-types';

class BuurgerIngredient extends Component{

    render(){
        let BI= null;
    switch(this.props.type){
         case ('burger-top'):
         BI=(
         <div className={BurgerIngredient.BreadTop}>
         <div className={BurgerIngredient.Seeds1}/>
         <div className={BurgerIngredient.Seeds2}/>

         </div>)
         break;
         case ('burger-bottom'):
         BI=<div className={BurgerIngredient.BreadBottom}>
         </div>
         break;
         case ('cheese'):
         BI=<div className={BurgerIngredient.Cheese}>
         </div>
         break;
         case ('meat'):
         BI=<div className={BurgerIngredient.Meat}>
         </div>
         break;
         case ('salad'):
         BI=<div className={BurgerIngredient.Salad}>
         </div>
         break;
         case ('bacon'):
         BI=<div className={BurgerIngredient.Bacon}>
         </div>
         break;
         
         default:
            BI=null;  
        }
        
        return (BI)
    }
}

BuurgerIngredient.proptypes={
    type:Proptypes.string.isRequired
}
export default BuurgerIngredient