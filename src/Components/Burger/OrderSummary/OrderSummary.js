import React,{Component} from 'react'
import Aux from '../../../HOC/Aux';
import Button from '../../UI/Button/Button'


class OrderSummary extends Component{
    componentWillUpdate(){
        console.log("Now updated");
    }
render(){
    let placedIngredients=Object.keys(this.props.ingredients)
    .map(isKey=>{
        return <li key={isKey} > {isKey} : {this.props.ingredients[isKey]}</li>
    });
    
    return(
        <Aux>
        <div><strong>Your Order</strong></div>
        <div>A delicious burger with the following ingredients:</div>
        <p>this.
            <ul>{placedIngredients}</ul>
        </p>
    
        <p>Your order costs you:<strong>Rs.{this.props.tCost}</strong></p>
        <p>Proceed to check out?</p>
            <Button whenClicked={this.props.clickOut} buttonType="Danger">Cancel</Button>
        
            <Button whenClicked={this.props.clickAlert} buttonType="Success">Continue</Button>
    
    </Aux>)
}
    
    
    }



export default OrderSummary;