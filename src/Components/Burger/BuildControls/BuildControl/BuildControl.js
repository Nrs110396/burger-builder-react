import React from 'react'
import estyles from './BuildControl.css'
const buildControl =(props)=>
(
    <div className={estyles.BuildControl}>
    <div className={estyles.Label}>{props.label}</div>
    <button className={estyles.Less}
    onClick={props.removed}
    disabled={props.disabled[props.type ]}
    >Less</button>
    
    <button className={estyles.More}
    onClick={
        ()=>props.added(props.type)
    }
    >More</button>

    </div>
)
export default buildControl;