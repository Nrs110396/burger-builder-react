import React from 'react';
import estyles from './BuildControls.css';
import BuildControl from './BuildControl/BuildControl'
const control = [
    {label:'Salad' , type:'salad'},
    {label:'Meat' , type:'meat'},
    {label:'Bacon' , type:'bacon'},
    {label:'Cheese' , type:'cheese'}
]
const buildControls = (props)=>(
<div className={estyles.BuildControls}>
<p>Your burger cost you <strong>{props.price.toFixed(2)}Rs</strong></p>
    {
        control.map(ctrl=>{
          return ( 
          <BuildControl 
            label={ctrl.label}
            type={ctrl.type} 
            added={props.added}
            removed={()=>props.removed(ctrl.type)}
            disabled={props.disabledInfo}
            key={ctrl.label}
            />
               )
                         }
                    )
    }
    <button disabled={!props.purchased}
    onClick={props.showed}
    className={estyles.OrderButton}>{ props.isAuth?'ORDER NOW':'Sign In And Order'}</button>
</div>

)
 export default buildControls;