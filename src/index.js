import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import {BrowserRouter} from 'react-router-dom';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import {createStore,  applyMiddleware,compose,combineReducers} from 'redux';

import Reducer from './store/reducers/BurgerBuilder';
import authReducer from './store/reducers/Auth';
import OrderReducer from './store/reducers/Orders'
const comRed = combineReducers({
    auth :authReducer,
    burgerR:Reducer,
    orderR :OrderReducer
    });

const chromeExt = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store=createStore(comRed,  chromeExt(applyMiddleware(thunk)));
const nrs= <Provider store={store}><BrowserRouter><App/></BrowserRouter></Provider>
ReactDOM.render( nrs , document.getElementById('root'));
registerServiceWorker();
