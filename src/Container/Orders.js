 import React,{Component} from 'react';
 import Order from '../Components/Order/Order';
//  import {Route} from 'react-router-dom';
//  import axios from '../axios-or'
import * as aCT from '../store/actions/index'
import wEH from '../HOC/withErrorHandler/withErrorHandler'
 import {connect} from 'react-redux';
 class Orders extends Component{
    //  state={
    //      orderS : [],
    //      loading:true,
    //  }
    componentWillMount(){
        this.props.OrderTab(this.props.token,this.props.userId);
    //     axios.get('/orders.json')
    // .then(res=>{
    //     let arrey= []
    //     for(let key in res.data){
    //         arrey.push(
    //             { 
    //                 ...res.data[key],
    //                 id : key 
    //         }
    //      )
    //      console.log(res.data)
    //     }
       
    //     this.setState({orderS: arrey})
       
    // }).catch(err=>{})
    }
    render(){
        let ordersss=<h2>Loading....</h2>;
        if(this.props.orderS){

            ordersss=<div>
            {
                    
                this.props.orderS.map(p=>
                <Order key={p.id} ingredients={p.ingredient} tprice={p.price}/>)
                }</div>
        }
        return(
            <div>
                
                {ordersss}
                </div>
        )
    }
 }
 const stateToProps =(state)=>{
return{
    orderS:state.orderR.orders,
    token:state.auth.token,
    userId:state.auth.userId,
}
 }
 const actionToProps=(dispatch)=>{
     return{
         OrderTab:(token,userId)=>dispatch(aCT.OrderFetchOrder(token,userId)),
     }
 }

 export default connect(stateToProps,actionToProps)(Orders);
