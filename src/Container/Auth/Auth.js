import React,{Component} from 'react';
import Innpuut from '../../Components/Input/Input';
import Button from '../../Components/UI/Button/Button';
import eStyles from './Auth.css';
import {connect} from 'react-redux';
import * as aCt from '../../store/actions/index';
import { accessSync } from 'fs';

import {Redirect} from 'react-router-dom';
import Loading from '../../Components/UI/loading/loading';

class Auth extends Component{
    state={
        SignUpSignIn:true,

        controls:{
            email:{  
                touched :false,
                    elemType : 'input',
                            eleConfig:{
                                type:'email',
                                placeholder:'Your Email-Id',

                            },
                            validation:{
                                required:true,
                                isEmail:true,
                            },
                    val:' ',
                    valid:false,
                },

                password:{  
                    touched :false,
                        elemType : 'input',
                                eleConfig:{
                                    type:'password',
                                    placeholder:'Password',

                                },
                                validation:{
                                    minVal:7,
                                    required:true
                                },
                        val:' ',
                        valid:false,
                    },
        }
    }
    componentDidMount(){
        if(!this.props.building && this.props.authRedirect!=='/' ){
            this.props.onSetAuthRedirect();
        }
    }

    checkValidity=(value,test)=>{
        let isRequired = true;
        
        if(test.required){
            isRequired= value.trim() !=='' && isRequired;
        }
       
        if( test.maxVal && isRequired){
            isRequired= value.length  <= test.maxVal && isRequired;
        }
        if( test.minVal   && isRequired){
           isRequired= value.length >= test.minVal && isRequired;
       }
       console.log(this.state.controls)
        return isRequired;
        
       
       }

       onFormChange=(event,cntrName)=>{
           const updatedForm ={
               ...this.state.controls,
               [cntrName]:{
                  
                ...this.state.controls[cntrName],
                val:event.target.value,
                valid:this.checkValidity(event.target.value,
                    this.state.controls[cntrName].validation) ,
                    touched :true
            }
           
           }
           console.log(updatedForm);
           this.setState({controls:updatedForm})
}

signInSwitch=()=>{
this.setState(
    prevState =>{
        return{
            SignUpSignIn:!prevState.SignUpSignIn,
        };
    }
)
}

submitHandler=(event)=>{
    event.preventDefault();
    this.props.onSubmit(this.state.controls.email.val,
        this.state.controls.password.val,
        this.state.SignUpSignIn);
}

render(){
    let emtArray =[];

    for(let keyss in this.state.controls){
        emtArray.push({
            id: keyss,
            eleValue:this.state.controls[keyss]
        })
        
    }

   let errorMessage = null;
    if (this.props.error){
        errorMessage=<h2>{this.props.error}</h2>
    }

     let bbqn= <div> {
         emtArray.map(i=>{ 
         return( <Innpuut key={i.id} 
     
     elementttype={i.eleValue.elemType} 
     
     elementtconfig={i.eleValue.eleConfig}
     isInValid={i.eleValue.valid}
     touched={i.eleValue.touched}
     ShouldValidateForDropdown={i.eleValue.validation.required}
     cChanged={(event)=>this.onFormChange(event,i.id)}
     
     value={i.eleValue.value} {...emtArray.eleValue}/>)
    }                   )} </div>

if(this.props.loading){
    bbqn=<Loading/>
}
let formShow =null;
if(this.props.isAuthenticated){
   formShow=<Redirect to={this.props.authRedirect}/> 
}

return(

<div className={eStyles.Auth}>
{formShow}
{errorMessage}
<form onSubmit={this.submitHandler}>
{bbqn}

<Button buttonType='Success'>Submit</Button>
</form>
<Button buttonType='Danger' 
whenClicked={this.signInSwitch}>Switch to {this.state.SignUpSignIn ? 'Sign In':'Sign Up'}
</Button>

</div>
)
}

}
const mapStateToProps =(state)=>{
    return{
        loading:state.auth.loading,
        error:state.auth.error,
        isAuthenticated : state.auth.token,
        authRedirect:state.auth.authRedirect,
        building:state.burgerR.building,
    }
}

const mapDispatchToProps=(dispatch)=>{
        return{
            onSubmit:(e,p,signUp)=>dispatch(aCt.auth(e,p,signUp)),

            onSetAuthRedirect:()=>dispatch(aCt.setAuthRedirect('/'))
        } 
}
export default connect(mapStateToProps,mapDispatchToProps) (Auth);