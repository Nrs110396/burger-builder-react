import React,{Component} from 'react';
import Aux from '../../HOC/Aux';
import Burger from '../../Components/Burger/Burger';
import BuildControls from '../../Components/Burger/BuildControls/BuildControls';
import Modal from '../../Components/UI/Modal/Modal';
import OrderSummary from '../../Components/Burger/OrderSummary/OrderSummary'

import axioss from '../../axios-or'

import {connect} from 'react-redux';
import * as actionT from '../../store/actions/index';

import Loading from '../../Components/UI/loading/loading'
import withErrorHandler from '../../HOC/withErrorHandler/withErrorHandler'
// import axioss from 'axios'
// import ToolBar from '../../Components/Navigation/Toolbar/Toolbar'



class BB extends Component{
state={
   
    total:40,
    // purchased:false,
    purchasing :false,
    loadingg:false,
  
}

updaterFun=(newObj)=>  {
    // const oldObj= {...this.state.ingredients};
    const sum = Object.keys(newObj).map(isKey=>{
        return newObj[isKey];
    }).reduce((sum,ele)=>{
        return sum+ele;
    },0);

    return sum>0;
}

purchaseHandler =()=>{
   if(this.props.isAuthenticted)
   {
    this.setState(
        {purchasing:true}
    );
   }
   else{
       this.props.onSetAuthRedirectPath('/checkout');
       this.props.history.push('/auth');
   }
}

    //  adding=(type)=>{
//Dressing  

//         const newObj = {...this.state.ingredients};
//         const oldValue = this.state.ingredients[type];
//         const newValue = oldValue +1;
//         newObj[type]=newValue;
// //Cost Eval
//         // const totalPrice = this.state.total;
//         // const newFillingPrice = Price[type] + totalPrice;
        
//         this.setState(
//             {
//                 ingredients: newObj,
//                 total: newFillingPrice,

//             }
//         )
//         this.updaterFun(newObj);
//     }

//     removing=(type)=>{
//         //Dressing  
//                 const newObj = {...this.state.ingredients};
//                 const oldValue = this.state.ingredients[type];
//                 if (oldValue <=0){
//                     return;
//                 }
//                 const newValue = oldValue - 1;
//                 newObj[type]=newValue;
//         //Cost Eval
//                 const totalPrice = this.state.total;
//                 const newFillingPrice =   totalPrice -Price[type];
//                 this.setState(
//                     {
//                         ingredients: newObj,
//                         total: newFillingPrice,
//                     }
//                 )
        
//         this.updaterFun(newObj);
                
//              }
    
//      purchaseHandler =()=>{
//                 this.setState(
//                     {purchasing:true}
//                 );
//             }


    closeSummary =()=>{
        this.setState(
            {purchasing:false}
        );
    }

    clickAlertt=()=>{
        this.props.history.push('checkout');
        this.props.onPurchasedDone()
    //     this.setState({loadingg:true})
    //     let nu={...this.state.ingredients}
    //     const onlineIngredients=
    //     {
    //          ingre:nu,
            
    //                 }
        
    //     axioss.post('/orders.json',onlineIngredients)
    //     .then( (res)=>
    //     this.setState({loadingg:false,purchasing:false})
    
    // )
    //     .catch(error=>
    //         this.setState({loadingg:false,purchasing:false})
        
    //     );
    //    const queryParams= [];
    //     for(let i in this.props.ingre){
    //        queryParams.push(i+'='+
    //        encodeURIComponent(this.state.ingredients[i]));
    //     //    console.log(typeof queryParams[0] ) string in array-  ' bacon=1'
    //     }        
    //     queryParams.push('total='+this.state.total)
    //     let queryString = queryParams.join('&')
    //         this.props.history.push({
    //             pathname:'/checkout',
    //             search: '?' + queryString
    //         })
                        }

    componentWillMount(){
    // axioss.get('https://burger-builder-86930.firebaseio.com/ingredients.json')
    // .then(res=>{this.setState({ingredients: res.data,})})
    // .catch(error=>{
    //     this.setState({errors:true})
    // })
    this.props.onFetch()
    
    }
    componentDidMount=()=>{

    }

     
    render(){
        const disabledInfo = {...this.props.ingre}
        for (let element in disabledInfo)
        {
            disabledInfo[element]= disabledInfo[element] <=0;
        }

        let modal= <Loading/>

        if(this.state.loadingg ){
            modal = <div><Loading/><Loading/></div>
        }
        let burgerIn = this.props.error ? <p>Cant Load patty</p> : <Loading/>
        if(this.props.ingre){
            burgerIn=(<div><Burger ingredients={this.props.ingre}/>
                    <BuildControls 
                        added={this.props.onAdder} 
                        removed={this.props.onRemover}
                
                        showed={this.purchaseHandler}
                
                        isAuth={this.props.isAuthenticted}
                        price={this.props.total}
                        purchased={this.updaterFun(this.props.ingre)}


                        disabledInfo={disabledInfo}/>
            </div>);

            modal=<OrderSummary ingredients={this.props.ingre}
            clickOut ={this.closeSummary} 
            clickAlert={this.clickAlertt}
            tCost={this.state.total}/>
        }

return(
    <Aux>
        <Modal show={this.state.purchasing} 
        clickOut ={this.closeSummary}>

       {modal}

        </Modal>
        
        {burgerIn}
    </Aux>
);

}
}

const mapStateToProps=state=>{
    
    return{
        ingre: state.burgerR.ingredient,
        total: state.burgerR.total,
        error:state.burgerR.error,
        isAuthenticted: state.auth.token !== null,
        
    }
}

const disToProps =(dispatch)=>{
    return{
        onAdder: (ingName)=>dispatch(
            actionT.addIngre(ingName)),
        
        onRemover: (ingName)=>dispatch(
            actionT.removeIngre(ingName)),
        
        onFetch :()=>dispatch(actionT.ingreFetcher()),

        onPurchasedDone :()=>dispatch(actionT.onPurchasedDone()),

        onSetAuthRedirectPath : (path)=>dispatch(actionT.setAuthRedirect(path)),

    }
}

export default connect(mapStateToProps,disToProps)(withErrorHandler(BB,axioss)) ;