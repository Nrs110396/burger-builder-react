import React,{Component} from 'react';
import Button from '../../../Components/UI/Button/Button'
import eStyles from './ContactData.css'
import axioss from '../../../axios-or'
import Loading from '../../../Components/UI/loading/loading'
import Innpuut from '../../../Components/Input/Input'
import {connect} from 'react-redux';
import * as actionCre from '../../../store/actions/index';
import wEH from '../../../HOC/withErrorHandler/withErrorHandler';
class ContactData extends Component{
    state={
        ingredients:null,
    // loading:false,
    finalFormCheck : false,
    formData:{
                name:{  
                    touched :false,
                        elemType : 'input',
                                eleConfig:{
                                    type:'text',
                                    placeholder:'Your Name',

                                },
                                validation:{
                                    required:true
                                },
                        val:' ',
                        valid:false,
                    },
                street:{
                    touched :false,

                    elemType : 'input',
                            eleConfig:{
                                type:'text',
                                placeholder:'Your address',

                            },
                            validation:{
                                required:true
                            },
                    val:'',
                    valid:false,

                    },
                zipcode:{
                    touched :false,

                    elemType : 'input',
                            eleConfig:{
                                type:'text',
                                placeholder:'zip Code',

                            },
                            validation:{
                                minVal:6,
                    maxVal:6,
                                required:true
                            },
                    val:'',
                    valid:false,
                    

                    },
                country:{
                    touched :false,

                    elemType : 'input',
                            eleConfig:{
                                type:'text',
                                placeholder:'Your Country',

                            },
                            validation:{
                                required:true
                            },
                    val:'',
                    valid:false,

                    },
                email:{
                    touched :false,

                    elemType : 'input',
                            eleConfig:{
                                type:'email',
                                placeholder:'Email',

                            },
                            validation:{
                                required:true
                            },
                    val:'',
                    valid:false,
                    
                    },
                deliveryMode:{
                    touched :false,

                    elemType : 'select',
                            eleConfig:{
                               options:[
                                    { value:'fastest',dispValue:'fastest'},
                                    { value:'cheapest',dispValue:'cheapest'}

                               ]

                            },
                            validation:{
                                 
                            },
                      valid:true,     
                    val:'fastest',
                    },
                
    }
    }
    // componentWillMount(){
    //     this.setState({ingredients:this.props.ingredients});
       
    // }
    onSubmission=(event)=>{
                event.preventDefault();
               
                let formDataHolder = {...this.state.formData}
                let tempEle = {};

                for(let i in formDataHolder){
                    tempEle[i] = formDataHolder[i].val;
                }          

                const onlineIngredients=
                        {
                            ingredient : this.props.ingre ,
                            form:tempEle,
                            price:this.props.Thetotal ,
                            userId:this.props.userId
                                    }
                this.props.onSubmissionClick(onlineIngredients,this.props.token)
                // axioss.post('/orders.json',onlineIngredients)
                //             .then( (res)=>{
                //                 this.setState({loading:false})
                //                 this.props.history.push('/')
                //             }
                            
                        
                //         )
                //             .catch(error=>
                //                 this.setState({loading:false})
                            
                //             );
                console.log(this.props.ingre+this.props.total);
            }
//end
checkValidity=(value,test)=>{
 let isRequired = true;
 
 if(test.required){
     isRequired= value.trim() !=='' && isRequired;
 }

 if( test.maxVal && isRequired){
     isRequired= value.length  <= test.maxVal && isRequired;
 }
 if( test.minVal   && isRequired){
    isRequired= value.length >= test.minVal && isRequired;
}
console.log(this.state.formData)
 return isRequired;
 

}

    onFormChange=(event,ID)=>{
                let tempOne ={...this.state.formData} 
                let tempTwo = {...this.state.formData[ID]}
                tempTwo.val = event.target.value;
                tempTwo.touched=true;
                tempTwo.valid = this.checkValidity(tempTwo.val ,tempTwo.validation);
                tempOne[ID] = {...tempTwo} 

                let finalCheck = true;
                console.log(finalCheck);
                for(let i in tempOne){
                    console.log(tempOne[i].valid );
                    finalCheck= tempOne[i].valid && finalCheck;
                }
       

                this.setState({formData:tempOne,finalFormCheck:finalCheck})
               
    }

render(){
    let emtArray =[]

    for(let keyss in this.state.formData){
        emtArray.push({
            id: keyss,
            eleValue:this.state.formData[keyss]
        })
        
    }

     let bbqn=  <form onSubmit={this.onSubmission}>
         <div> {emtArray
        .map(i=>{return( 
        
     <Innpuut key={i.id} 
     
     elementttype={i.eleValue.elemType} 
     
     elementtconfig={i.eleValue.eleConfig}
     isInValid={i.eleValue.valid}
     touched={i.eleValue.touched}
     ShouldValidateForDropdown={i.eleValue.validation.required}
     cChanged={(event)=>this.onFormChange(event,i.id)}
     
     value={i.eleValue.value} {...emtArray.eleValue}/>)
    }                   )} </div>
    <Button buttonType='Success' disabled={!this.state.finalFormCheck}>Submit</Button>
    </form>

    
        {/* <Innpuut  type='input' name='Name' placeholder='Name'/>
        <Innpuut  type='email' name='Email' placeholder='Email'/>
        <Innpuut  type='textArea' name='Street' placeholder='Street'/>
        <Innpuut  type='selector' name='Postal' placeholder='Postal'/>
        <Button buttonType='Success' whenClicked={this.onSubmit}>Submit</Button></div> */}
       
        if (this.props.loading){
            bbqn = <Loading/>
        }
    return(
        <div className={eStyles.ContactData}>
        <h4>Fill up all the details-</h4>
            {bbqn}
            
        </div>
    )
}

}

 
const mapPropstoState=(state)=>{
    return{
        ingre:state.burgerR.ingredient,
        Thetotal:state.burgerR.total,
        loading:state.orderR.loading,
        token:state.auth.token,
        userId:state.auth.userId,
    }
}

const diapToprops=(dispatch)=>{
    return{
        onSubmissionClick : (orderData,token)=>dispatch(actionCre.realPurchaser(orderData,token))
    }
}
export default connect(mapPropstoState,diapToprops)(ContactData);