import React,{Component} from 'react';
import axios from 'axios';
import {Route,Redirect} from 'react-router-dom';
import ChSumm from '../../Components/Order/CheckoutSummary/CheckoutSummary'
import DataP from  './ContactData/ContactData'

import {connect} from 'react-redux';
class Checkout extends Component{

// componentWillUpdate(){
//     axios.get('https://burger-builder-86930.firebaseio.com/orders/-LKwWl6ijH8Q74XlKBsf/ingre.json')
// .then(res=>{
//     this.setState({ingredients:res.data})
//     console.log( res.data)}
// )
// }

// componentWillMount(){
//     console.log(this.props);
//     const query = new URLSearchParams(this.props.location.search);
     
//     const ing = {}
//     for(let i of query.entries()){
//         if(i[0]=== 'total'){
//             this.setState({total: i[1]})
//         }else{
//         ing[i[0]] = +i[1]; 

//         }
//         console.log(i)
//     }
//     this.setState({ingredients:ing})
// }
cancelHandler=()=>{
    this.props.history.goBack();
}
confirmHandler=()=>{
    this.props.history.replace('/checkout/contact-data')
}
render(){
    let cSumm = <h2><Redirect to='/'/></h2>
    if(this.props.ingre){
        let afterPurchased =  this.props.purchased ? <Redirect to='/'/> : null;
        console.log(afterPurchased)
        cSumm=<div>
            {afterPurchased}
            <ChSumm ingredients={this.props.ingre} 
        cancelH={this.cancelHandler}
        confirmH={this.confirmHandler}
        />
        <Route 
        path={this.props.match.url +'/contact-data' } 
        component={DataP}
        
         
        /></div>;

    }
    return(
        <div>
        <div>{cSumm}</div>
        
        </div>
        
    )
}


}
 
const mapPropstoState=(state)=>{
    return{
        ingre:state.burgerR.ingredient,
        purchased:state.orderR.purchased,
    }
}

export default connect(mapPropstoState)(Checkout);