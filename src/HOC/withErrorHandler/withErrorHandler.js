import React,{Component} from 'react';
import Aux from '../Aux';
import Modal from '../../Components/UI/Modal/Modal';


const withErrorHandler =(Wrapped,axios)=>{
    return class extends Component{
        state={error:null};
        componentWillMount(){
            console.log('nmounted')

            this.reqInter=axios.interceptors.request.use(req=>{
                
                this.setState({error:null});
                return req;
            
            
            })
            this.resInter=axios.interceptors.response.use(res=>res,error=>{
                this.setState({error:error});
            })
        }
        componentWillUnmount(){
            console.log('unmounted')
            axios.interceptors.request.eject(this.reqInter)
            axios.interceptors.response.eject(this.resInter)

        }

        showChanger=()=>{
            this.setState({error:null})
        }
        render (){ 
                  
        return(
            <Aux>
                <Modal show={this.state.error} clickOut={this.showChanger}>
                    {this.state.error ? this.state.error.message : null }
                </Modal>
                <Wrapped {...this.props}/>
            </Aux>
        )}
    }

}

export default withErrorHandler